#include "pgmlib.h"
#include <math.h>
#include <stdio.h>
int Cij[3][3] = /* Cij[j][i] */
  {{1, 1, 1}, {1, 1, 1}, {1, 1, 1}};
double K = 1 / 9.0;
int calc(int source, int x, int y)
{
  int value = 0;
  x--;
  y--;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      value += (image[source][x + i][y + j] * Cij[i][j]);
  return value * K;
}
void smooth(int n1, int n2)
{
  int x, y;
  int value;
  int min = 255;
  int max = 0;
  for (y = 1; y < height[n1] - 1; y++)
  {
    for (x = 1; x < width[n1] - 1; x++)
    {
      if (min > image[n1][x][y])
        min = image[n1][x][y];
      if (max < image[n1][x][y])
        max = image[n1][x][y];
    }
  }
  for (y = 1; y < height[n1] - 1; y++)
  {
    for (x = 1; x < width[n1] - 1; x++)
    {
      value           = calc(n1, x, y);
      image[n2][x][y] = (( double )(value - min) / ( double )(max - min)) * 255;
    }
  }

  for (y = 0; y < height[n2]; y++)
  {
    for (x = 0; x < width[n2]; x++)
    {
      int xsft, ysft;
      xsft = 0;
      ysft = 0;
      if (x == 0)
        xsft = 1;
      else if (x == width[n2] - 1)
        xsft = -1;

      if (y == 0)
        ysft = 1;
      else if (y == height[n2] - 1)
        ysft = -1;
      if (xsft != 0 || ysft != 0)
        image[n2][x][y] = image[n2][x + xsft][y + ysft];
    }
  }
}

int main(int argc, char const *argv[])
{
  load_image(0, "./circle.pgm");
  width[1]  = width[0];
  height[1] = height[0];
  smooth(0, 1);
  save_image(1, "circle_avg.pgm");
  return 0;
}
