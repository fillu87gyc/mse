#include "./pgmlib.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
double Uniform()
{
  return (( double )rand() + 1.0) / (( double )RAND_MAX + 2.0);
}
void salt_pepper(int n, double level)
{
  srand(( unsigned )time(NULL));
  for (int x = 0; x < width[n]; x++)
    for (int y = 0; y < height[n]; y++)
    {
      if (Uniform() < level)
        image[n][x][y] = 255;
      if (Uniform() < level)
        image[n][x][y] = 0;
    }
}
void add_gaussian_noise(int n, double mu, double sigma)
{
  double tmp, g_ns;
  for (int y = 0; y < height[n]; y++)
    for (int x = 0; x < width[n]; x++)
    {
      tmp  = sqrt(-2.0 * log(Uniform())) * sin(2.0 * M_PI * Uniform());
      g_ns = mu + sigma * tmp;
      if (image[n][x][y] + ( int )g_ns < 0)
        image[n][x][y] = 0;
      else if (image[n][x][y] + ( int )g_ns > 255)
        image[n][x][y] = 255;
      else
        image[n][x][y] = image[n][x][y] + ( int )g_ns;
    }
}
void median(int n1, int n2)
{
  int x, y, i, j;
  int neighbour[9], flag[9];
  int xsft, ysft;

  width[n2]  = width[n1];
  height[n2] = height[n1];
  init_image(n2, 0); /* 画像No.n2を0で初期化 */
  for (y = 1; y < height[n1] - 1; y++)
  {
    for (x = 1; x < width[n1] - 1; x++)
    {
      int count = 0;
      for (j = -1; j < 2; j++)
      {
        for (i = -1; i < 2; i++)
        {
          //取得
          neighbour[count] = image[n1][x + i][y + j];
          flag[count]      = 0; /* =0:未処理，=1:選択済み を表す */
          count++;
        }
      }
      int min_temp = 255;    //こっちは具体的数字
      int minnum   = 0;    //こっちはインデックスを取る
      for (i = 0; i < 5; i++)
      {
        //　Medianフィルターは5番目をその画素にするため5回
        min_temp = 255;
        minnum   = 0;
        for (j = 0; j < 9; j++)
        {
          if (flag[j] == 0 && neighbour[j] <= min_temp)
          {
            min_temp = neighbour[j];
            minnum   = j;
          }
        }
        flag[minnum] = 1; /* 5番目として選択済みを表す */
      }
      // 5番目の数字を突っ込む
      image[n2][x][y] = min_temp;
    }
  }
  /* 外周部の補間 */
  for (y = 0; y < height[n2]; y++)
  {
    for (x = 0; x < width[n2]; x++)
    {
      xsft = 0;
      ysft = 0;
      if (x == 0)
        xsft = 1;
      else if (x == width[n2] - 1)
        xsft = -1;
      if (y == 0)
        ysft = 1;
      else if (y == height[n2] - 1)
        ysft = -1;
      if (xsft != 0 || ysft != 0)
        image[n2][x][y] = image[n2][x + xsft][y + ysft];
    }
  }
}
int calc(int source, int x, int y)
{
  int value = 0;
  x--;
  y--;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      value += image[source][x + i][y + j];
  return value / 9;
}

void smooth(int n1, int n2)
{
  width[n2]  = width[n1];
  height[n2] = height[n1];
  int x, y;
  int value;
  int min = 255;
  int max = 0;
  for (y = 1; y < height[n1] - 1; y++)
  {
    for (x = 1; x < width[n1] - 1; x++)
    {
      if (min > image[n1][x][y])
        min = image[n1][x][y];
      if (max < image[n1][x][y])
        max = image[n1][x][y];
    }
  }
  for (y = 1; y < height[n1] - 1; y++)
  {
    for (x = 1; x < width[n1] - 1; x++)
    {
      value           = calc(n1, x, y);
      image[n2][x][y] = (( double )(value - min) / ( double )(max - min)) * 255;
    }
  }

  for (y = 0; y < height[n2]; y++)
  {
    for (x = 0; x < width[n2]; x++)
    {
      int xsft, ysft;
      xsft = 0;
      ysft = 0;
      if (x == 0)
        xsft = 1;
      else if (x == width[n2] - 1)
        xsft = -1;

      if (y == 0)
        ysft = 1;
      else if (y == height[n2] - 1)
        ysft = -1;
      if (xsft != 0 || ysft != 0)
        image[n2][x][y] = image[n2][x + xsft][y + ysft];
    }
  }
}

double minimizing_mean_square_error(int n1, int n2)
{
  int MSE = 0;
  for (int x = 0; x < width[n1]; x++)
  {
    for (int y = 0; y < height[n1]; y++)
    {
      int tmp = image[n1][x][y] - image[n2][x][y];
      // printf("%d\n", tmp * tmp);
      MSE += tmp * tmp;
    }
  }
  return MSE / ( double )(width[n1] * height[n1]);
}

int main(int argc, char const *argv[])
{
  printf("\n");
  load_image(0, "./lenna.pgm");
  load_image(1, "./lenna.pgm");
  double lv = 0.05;
  // scanf("%lf", &lv);
  salt_pepper(1, lv);
  smooth(1, 2);
  median(1, 3);
  printf("salt   %f\n", minimizing_mean_square_error(0, 1));
  save_image(1, "salt.pgm");
  printf("smooth %f\n", minimizing_mean_square_error(0, 2));
  save_image(2, "smooth.pgm");
  printf("median %f\n", minimizing_mean_square_error(0, 3));
  save_image(3, "median.pgm");


  printf("\n----------\n\n");

  load_image(0, "./circle.pgm");
  load_image(1, "./circle.pgm");
  add_gaussian_noise(1, 0, 15);
  smooth(1, 2);
  median(1, 3);
  printf("gaussian %f\n", minimizing_mean_square_error(0, 1));
  save_image(1, "gaussian.pgm");
  printf("smooth   %f\n", minimizing_mean_square_error(0, 2));
  save_image(2, "gaussian_smooth.pgm");
  printf("median   %f\n", minimizing_mean_square_error(0, 3));
  save_image(3, "gaussian_median.pgm");

  return 0;
}
