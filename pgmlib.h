#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_IMAGESIZE 1280 /* Max image size*/
#define MAX_BRIGHTNESS 255 /* Max value of intensity*/
#define GRAYLEVEL 256 /* Max gray level number = max value of intentity + 1 */
#define MAX_FILENAME 256 /* Max length of file name*/
#define MAX_BUFFERSIZE 256 /* Max size of buffer */
#define MAX_NUM_OF_IMAGES 5 /* Max number of images*/

/* Global variable*/
unsigned char image[MAX_NUM_OF_IMAGES][MAX_IMAGESIZE][MAX_IMAGESIZE];
int width[MAX_NUM_OF_IMAGES], height[MAX_NUM_OF_IMAGES];
void load_image(int n, const char name[]);
void save_image(int n, const char name[]);
void copy_image(int n1, int n2);
void init_image(int n, unsigned char value);

void load_image(int n, const char name[])
{
  char file_name[MAX_FILENAME];    // char array for file name
  char buffer[MAX_BUFFERSIZE];    // char array for reading image data
  FILE *fp;    // file point
  int max_gray;    // maximum of gray level
  int x, y;    // loop viarable

  if (name[0] == '\0')
  {
    printf("Input the file name (*.pgm):");
    scanf("%s", file_name);
  }
  else
    strcpy(file_name, name);
  if ((fp = fopen(file_name, "rb")) == NULL)
  {
    printf("No such image file\n");
    exit(1);
  }
  fgets(buffer, MAX_BUFFERSIZE, fp);
  if (buffer[0] != 'P' || buffer[1] != '5')
  {
    printf("The image file is not P5 pgm format\n");
    exit(1);
  }
  width[n]  = 0;
  height[n] = 0;
  while (width[n] == 0 || height[n] == 0)
  {
    fgets(buffer, MAX_BUFFERSIZE, fp);
    if (buffer[0] != '#')
      sscanf(buffer, "%d %d", &width[n], &height[n]);
  }
  max_gray = 0;
  while (max_gray == 0)
  {
    fgets(buffer, MAX_BUFFERSIZE, fp);
    if (buffer[0] != '#')
      sscanf(buffer, "%d", &max_gray);
  }
  // printf("image width = %d, image height = %d\n", width[n], height[n]);
  // printf("maximum of gray level = %d\n", max_gray);
  if (width[n] > MAX_IMAGESIZE || height[n] > MAX_IMAGESIZE)
  {
    printf("The image size is over the maximum%d,%d\n",
           MAX_IMAGESIZE,
           MAX_IMAGESIZE);
    printf("Plese select a smaller image\n");
    exit(1);
  }
  if (max_gray != MAX_BRIGHTNESS)
  {
    printf("The image is not 8-bit image. Please select an 8-bit image\n");
    exit(1);
  }
  for (y = 0; y < height[n]; y++)
    for (x = 0; x < width[n]; x++)
      image[n][x][y] = ( unsigned char )fgetc(fp);
  fclose(fp);
}

void save_image(int n, const char name[])
{
  char file_name[MAX_FILENAME];    // char array for file name
  FILE *fp;    // file point
  int x, y;    // loop variables

  if (name[0] == '\0')
  {
    printf("Output the file name (*.pgm):: ");
    scanf("%s", file_name);
  }
  else
    strcpy(file_name, name);
  if ((fp = fopen(file_name, "wb")) == NULL)
  {
    printf("Failure in opening file\n");
    exit(1);
  }
  fputs("P5\n", fp);
  fputs("# Created by Image Processing\n", fp);
  fprintf(fp, "%d %d\n", width[n], height[n]);
  fprintf(fp, "%d\n", MAX_BRIGHTNESS);
  for (y = 0; y < height[n]; y++)
    for (x = 0; x < width[n]; x++)
      fputc(image[n][x][y], fp);
  fclose(fp);
}

void copy_image(int n1, int n2)
{
  int x, y; /* loop variable*/

  width[n2]  = width[n1];
  height[n2] = height[n1];
  for (y = 0; y < height[n1]; y++)
    for (x = 0; x < width[n1]; x++)
      image[n2][x][y] = image[n1][x][y];
}

void init_image(int n, unsigned char value)
{
  int x, y; /* loop variable*/

  for (y = 0; y < height[n]; y++)
    for (x = 0; x < width[n]; x++)
      image[n][x][y] = value;
}
